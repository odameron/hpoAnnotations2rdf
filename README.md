# HPO annotations to RDF

Scripts for converting HPO (https://hpo.jax.org) annotations from http://purl.obolibrary.org/obo/hp/hpoa/phenotype.hpoa into RDF.
