#! /usr/bin/env python3

import argparse
import csv

evidence = {}
evidence["IEA"] = "eco:0000501"
evidence["PCS"] = "eco:0006017"
evidence["TAS"] = "eco:0000304"


def parsePhenotype(filepath):
	with open(filepath) as csvfile, open(filepath.replace('.hpoa', '.ttl'), 'w') as ttlfile:
		#ttlfile.write()
		ttlfile.write('@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\n')
		ttlfile.write('@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\n')
		ttlfile.write('@prefix owl: <http://www.w3.org/2002/07/owl#>.\n')
		ttlfile.write('@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\n')
		ttlfile.write('@prefix skos: <http://www.w3.org/2004/02/skos/core#>.\n')
		ttlfile.write('@prefix dcterms: <http://purl.org/dc/terms/> .\n')
		ttlfile.write('\n')
		ttlfile.write('@prefix eco: <http://purl.obolibrary.org/obo/ECO_>.\n')
		ttlfile.write('@prefix pubmed: <https://www.ncbi.nlm.nih.gov/pubmed/>.\n')
		ttlfile.write('@prefix taxon: <http://purl.uniprot.org/taxonomy/>.\n')
		ttlfile.write('@prefix sio: <http://semanticscience.org/resource/>.\n')
		ttlfile.write('\n')
		ttlfile.write('@prefix HP: <http://purl.obolibrary.org/obo/HP_>.\n')
		ttlfile.write('@prefix hpoa: <http://hpo.jax.org/annotation/>.\n')
		ttlfile.write('@prefix OMIM: <http://purl.bioontology.org/ontology/OMIM/>.\n')
		ttlfile.write('@prefix DECIPHER: <https://www.deciphergenomics.org/>.\n')
		ttlfile.write('@prefix ORPHA: <http://www.orpha.net/ORDO/Orphanet_>.\n')

		annotationReader = csv.reader(csvfile, delimiter='\t')

		lineNumber = 0
		hpoaDate = "2021-01-01"
		for row in annotationReader:
			if len(row) == 0:
				continue
			if row[0].startswith("#"):
				if row[0].startswith("#date: "):
					hpoaDate = row[0][len("#date: "):].replace("\n", "")
					ttlfile.write('\n# was generated from HPO annotation file version {} \n\n'.format(hpoaDate))
				continue

			ttlfile.write('\n')
			currentIdent = 'hpoa:annot-{}'.format(lineNumber)
			#print(currentIdent)
			if row[2] == "NOT":
				ttlfile.write('{} rdf:type hpoa:NegativeDiseasePhenotypeAnnotation .\n'.format(currentIdent))
			else:
				ttlfile.write('{} rdf:type hpoa:PositiveDiseasePhenotypeAnnotation .\n'.format(currentIdent))
			ttlfile.write('{} hpoa:disease {} .\n'.format(currentIdent, row[0]))
			ttlfile.write('{} hpoa:phenotype {} .\n'.format(currentIdent, row[3]))
			ttlfile.write('{} hpoa:evidence {} .\n'.format(currentIdent, evidence[row[5]]))
			if row[7] != "":
				ttlfile.write('{} hpoa:frequency "{}" .\n'.format(currentIdent, row[7]))
			if row[10] != "":
				ttlfile.write('{} hpoa:aspect hpoa:Aspect-{} .\n'.format(currentIdent, row[10]))

			lineNumber += 1


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Convert a Human Phenotype Ontology annotation file into RDF.')
	parser.add_argument("phenotypeFilePath", help="path to the phenotype annotation file")
	args = parser.parse_args()
	parsePhenotype(args.phenotypeFilePath)
